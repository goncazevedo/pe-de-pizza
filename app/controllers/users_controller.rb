class UsersController < ApplicationController
    before_action :find_user, only: [:show, :edit, :update, :destroy]
    before_action :block_unlogged, except: [:new, :create] 
    before_action :block_logged, only: [:new, :create]
    before_action :block_users, only: [:index]
    before_action :block_intruder,only: [:show, :create]


    def new
        @user = User.new
    end

    def edit
    end  

    def create
        @user =  User.new(user_params)

        if @user.save
            redirect_to root_path

        else
            render "new"
        end
    end

    def index
        @users = User.all
    end

    def show
        @user = User.find(params[:id])
    end

    def update
        if @user.update(user_params)
            redirect_to @user
        else
            render "edit"
        end
    
    end

    private

        def user_params
            params.require(:user).permit( :name, :email, :telephone, :password, :password_confirmation)
        end

        def find_user
            @user = User.find(params[:id])
        end

        def block_unlogged
            if !logged_in?
                redirect_to root_path
            end
        end
        
        def block_users
            if !is_admin?
                redirect_to root_path
            end
        end


        def block_logged
            if logged_in?
                redirect_to current_user
            end
        end

        def block_intruder
            if logged_in? && !(current_user.id == @user.id) && !is_admin?
                redirect_to current_user
            end
        end

end