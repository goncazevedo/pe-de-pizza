class SessionsController < ApplicationController
    before_action :impede_logado, only: [:new, :create]
    
    def  new
    end

    def create
        user = User.find_by(email: params[:email])
        if user && user.authenticate(params[:password])
            log_in(user)
            redirect_to user
        else
            redirect_to root_path

        end
    end

    def destroy
        log_out
        redirect_to root_path
    end

    private
        def impede_logado
            if logged_in?
                redirect_to current_user
            end
        end



end
