Rails.application.routes.draw do
  resources :products
  resources :users 
  patch "/users/:id", to: "users#update"
  put	"/users/:id", to: "users#update"
  get "/cadastro", to: "users#new"
  root "static_pages#home"
  get "/login", to: "sessions#new"
  post "/login", to: "sessions#create"
  delete "/logout", to: "sessions#destroy"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
